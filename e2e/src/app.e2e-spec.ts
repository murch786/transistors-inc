import { browser, by, element, logging } from 'protractor';

import { AppPage } from './app.po';

describe('Transistors Inc App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display 0.0 transistors', () => {
    page.navigateTo();
    browser.waitForAngularEnabled(false);
    expect(page.getTransistorAmount()).toEqual('0.0');
  });

  it('should earn 1.0 transistors after one 4 second round by default', () => {
    page.navigateTo();
    browser.waitForAngularEnabled(false);
    page.clickRoundStart();
    browser.sleep(4000);
    expect(page.getTransistorAmount()).toEqual('1.0');
    browser.sleep(500);
    expect(page.getTransistorAmount()).toEqual('1.0');
  });

  it('should earn 3.0 transistors after one 15 second round in increase R state', () => {
    page.navigateTo();
    browser.waitForAngularEnabled(false);
    page.clickReplicationState();
    page.clickRoundStart();
    browser.sleep(16000);
    expect(page.getTransistorAmount()).toEqual('3.0');
    browser.sleep(500);
    expect(page.getTransistorAmount()).toEqual('3.0');
  });

  it('should not crash after changing through all 3 states while running', () => {
    page.navigateTo();
    browser.waitForAngularEnabled(false);
    page.clickRoundStart();
    browser.sleep(500);
    page.clickReplicationState();
    browser.sleep(500);
    page.clickCapState();
    browser.sleep(500);
    page.clickReplicationState();
    browser.sleep(500);
    page.clickSpeedState();
    browser.sleep(500);
    page.clickCapState();
    browser.sleep(500);
    page.clickSpeedState();
    browser.sleep(2500);
    expect(page.getTransistorAmount()).toEqual('1.0');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
