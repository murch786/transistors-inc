// Converted from original C by HeinousTugboat. Original comments left intact for posterity's sake.

// Romu Pseudorandom Number Generators
//
// Copyright 2020 Mark A. Overton
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// ------------------------------------------------------------------------------------------------
//
// Website: romu-random.org
// Paper:   http://arxiv.org/abs/2002.11331
//
// Copy and paste the generator you want from those below.
// To compile, you will need to #include <stdint.h> and use the ROTL definition below.
// tslint:disable: no-bitwise comment-format

const ROTL32 = (d: number, lrot: number, size: number): number => (d << lrot) | (d >> (8 * size - lrot));
const ROTL = (d: bigint, lrot: bigint, size: bigint): bigint => (d << lrot) | (d >> (BigInt(8) * size - lrot));
const range16 = (n: number): number => ((n * 10 ** 16) / 2 ** 16) / 10 ** 16;
const range32 = (n: number): number => ((n * 10 ** 16) / 2 ** 32) / 10 ** 16;
const range64 = (n: bigint): number => Number((n * BigInt(10) ** BigInt(16)) / BigInt(2) ** BigInt(64)) / 10 ** 16;

export type seed32b = [number, number, number, number];
export type seed24b = [number, number, number];
export type seed16b = [number, number];

export interface BasePRNG<S extends number[]> {
  (): number;
  seed: S;
}

export interface PRNG<S extends number[]> extends BasePRNG<S> {
  init(s: number): void;
  range(s: number): number;
}

export interface RNG extends PRNG<[number, number]> { }

//===== RomuQuad ==================================================================================
//
// More robust than anyone could need, but uses more registers than RomuTrio.
// Est. capacity >= 2^90 bytes. Register pressure = 8 (high). State size = 256 bits.

export function romQuadRandom(initialSeed: [] | seed32b = []): BasePRNG<seed32b> {
  const size = 4;
  const buffer = new ArrayBuffer(8 * size);

  const seed = new Float64Array(buffer, 0, size);
  seed.forEach((_, i) => seed[i] = initialSeed[i] ?? Math.random());

  const state = new BigUint64Array(buffer, 0, size);

  const rand = (): number => {
    const [w, x, y, z] = state;
    state[0] = BigInt('15241094284759029579');
    state[1] = z + ROTL(w, BigInt(52), BigInt(4));
    state[2] = y - x;
    state[3] = y + w;
    state[3] = ROTL(state[3], BigInt(19), BigInt(4));
    return range64(x);
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [...seed]; }
  });

  return rand as BasePRNG<seed32b>;
}

//===== RomuTrio ==================================================================================
//
// Great for general purpose work, including huge jobs.
// Est. capacity = 2^75 bytes. Register pressure = 6. State size = 192 bits.

export function romTrioRandom(initialSeed: [] | seed24b = []): BasePRNG<seed24b> {
  const size = 3;
  const buffer = new ArrayBuffer(8 * size);

  const seed = new Float64Array(buffer, 0, size);
  seed.forEach((_, i) => seed[i] = initialSeed[i] ?? Math.random());

  const state = new BigUint64Array(buffer, 0, size);

  const rand = (): number => {
    const [x, y, z] = state;
    state[0] = BigInt('15241094284759029579') * z;
    state[1] = y - x;
    state[1] = ROTL(state[1], BigInt(12), BigInt(4));
    state[2] = z - y;
    state[2] = ROTL(state[2], BigInt(44), BigInt(4));
    return range64(x);
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [...seed]; }
  });

  return rand as BasePRNG<seed24b>;
}

//===== RomuDuo ==================================================================================
//
// Might be faster than RomuTrio due to using fewer registers, but might struggle with massive jobs.
// Est. capacity = 2^61 bytes. Register pressure = 5. State size = 128 bits.

export function romDuoRandom(initialSeed: [] | seed16b = []): BasePRNG<seed16b> {
  const size = 2;
  const buffer = new ArrayBuffer(8 * size);

  const seed = new Float64Array(buffer, 0, size);
  seed.forEach((_, i) => seed[i] = initialSeed[i] ?? Math.random());

  const state = new BigUint64Array(buffer, 0, size);

  const rand = (): number => {
    const [x] = state;
    state[0] = BigInt('15241094284759029579') * state[1];
    state[1] = ROTL(state[1], BigInt(36), BigInt(4)) + ROTL(state[1], BigInt(15), BigInt(4)) - x;
    return range64(x);
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [...seed]; }
  });

  return rand as BasePRNG<seed16b>;
}

//===== RomuDuoJr ================================================================================
//
// The fastest generator using 64-bit arith., but not suited for huge jobs.
// Est. capacity = 2^51 bytes. Register pressure = 4. State size = 128 bits.

export function romDuoJrRandom(initialSeed: [] | seed16b = []): PRNG<seed16b> {
  const size = 2;
  const buffer = new ArrayBuffer(8 * size);

  const seed = new Float64Array(buffer, 0, size);
  seed.forEach((_, i) => seed[i] = initialSeed[i] ?? Math.random());

  const state = new BigUint64Array(buffer, 0, size);

  const next = (): bigint => {
    const [x] = state;
    state[0] = BigInt('15241094284759029579') * state[1];
    state[1] = state[1] - x;
    state[1] = ROTL(state[1], BigInt(27), BigInt(4));
    return x;
  };

  const rand = (): number => {
    return range64(next());
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [...seed]; }
  });

  Object.defineProperty(rand, 'init', {
    value: (s: number): void => {
      state[0] = BigInt('0x9E6C63D0676A9A99');
      state[1] = ~BigInt(s) - BigInt(s);
      state[1] = state[1] * state[0];
      state[1] = state[1] ^ (state[1] >> BigInt(23) ^ state[1] >> BigInt(51));
      state[1] = state[1] * state[0];
      state[0] = state[0] * (state[1] << BigInt(27) | state[1] >> BigInt(37));
      state[1] = state[1] ^ (state[1] >> BigInt(23) ^ state[1] >> BigInt(51));
    }
  });
  // Fastrange provided by:
  //   https://lemire.me/blog/2016/06/27/a-fast-alternative-to-the-modulo-reduction/
  Object.defineProperty(rand, 'range', {
    value: (n: number): number => {
      return Number(((next() >> BigInt(32)) * BigInt(n)) >> BigInt(32));
    }
  });

  return rand as PRNG<seed16b>;
}

//===== RomuQuad32 ================================================================================
//
// 32-bit arithmetic: Good for general purpose use.
// Est. capacity >= 2^62 bytes. Register pressure = 7. State size = 128 bits.

export function romQuad32Random(initialSeed: [] | seed32b = []): BasePRNG<seed32b> {
  const size = 4;
  const buffer = new ArrayBuffer(4 * size);

  const state = new Uint32Array(buffer, 0, size);
  state.forEach((_, i) => state[i] = initialSeed[i] ?? Math.random() * 2 ** 32);


  const rand = (): number => {
    const [w, x, y, z] = state;
    state[0] = 3323815723 * z;
    state[1] = z + ROTL32(w, 26, size);
    state[2] = y - x;
    state[3] = y + w;
    state[3] = ROTL32(state[3], 9, size);
    return range32(x);
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [...state]; }
  });

  return rand as BasePRNG<seed32b>;
}

//===== RomuTrio32 ===============================================================================
//
// 32-bit arithmetic: Good for general purpose use, except for huge jobs.
// Est. capacity >= 2^53 bytes. Register pressure = 5. State size = 96 bits.

export function romTrio32Random(initialSeed: [] | seed24b = []): BasePRNG<seed24b> {
  const size = 3;
  const buffer = new ArrayBuffer(4 * size);

  const state = new Uint32Array(buffer, 0, size);
  state.forEach((_, i) => state[i] = initialSeed[i] ?? Math.random() * 2 ** 32);


  const rand = (): number => {
    const [x, y, z] = state;
    state[0] = 3323815723 * z;
    state[1] = y - x;
    state[1] = ROTL32(state[1], 6, 3);
    state[2] = z - y;
    state[2] = ROTL32(state[2], 22, 3);
    return range32(x);
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [...state]; }
  });

  return rand as BasePRNG<seed24b>;
}

//===== RomuMono32 ===============================================================================
//
// 32-bit arithmetic: Suitable only up to 2^26 output-values. Outputs 16-bit numbers.
// Fixed period of (2^32)-47. Must be seeded using the romuMono32_init function.
// Capacity = 2^27 bytes. Register pressure = 2. State size = 32 bits.

export function romMono32Random(initialSeed: [] | [number] = []): BasePRNG<[number]> {
  const size = 1;
  const buffer = new ArrayBuffer(4 * size);

  const state = new Uint32Array(buffer, 0, size);
  state[0] = ((initialSeed[0] ?? Math.random() * 2 ** 32) & 0x1fffffff) + 1156979152;
  const result = new Uint16Array(buffer, 0, size);

  const rand = (): number => {
    const x = result[0];
    state[0] *= 3611795771;
    state[0] = ROTL32(state[0], 12, 4);
    return range16(x);
  };

  Object.defineProperty(rand, 'seed', {
    get() { return [(state[0] - 1156979152) & 0x1fffffff]; }
  });

  return rand as BasePRNG<[number]>;
}
