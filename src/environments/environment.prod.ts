import { Environment } from 'src/app/environment';

import { version } from '../../package.json';

export const environment: Environment = {
  production: true,
  title: 'Transistors Inc',
  version
};
