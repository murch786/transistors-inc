import { InjectionToken } from '@angular/core';

import { environment } from '../environments/environment';

export interface Environment {
  production: boolean;
  title: string;
  version: string;
}

export const ENV = new InjectionToken<Environment>('Environment Config', {
  providedIn: 'root',
  factory: () => environment
});
