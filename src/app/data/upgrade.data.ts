import { BonusType } from '@transistors-inc/interfaces';

export const BonusTypeNames: { [key in BonusType]: string } = {
  [BonusType.M_ADD]: 'Cap Increase',
  [BonusType.M_MULT]: 'Cap Multiplier',
  [BonusType.R_ADD]: 'Replication Increase',
  [BonusType.R_MULT]: 'Replication Multiplier',
  [BonusType.T]: 'Cap Growth Multiplier',
};
