import { BonusType, Currency, Upgrade } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

const replicationUpgrades: Upgrade[] = [];
const capUpgrades: Upgrade[] = [];
const capGrowthUpgrades: Upgrade[] = [];

export function replication(level: Decimal): Upgrade {
  if (replicationUpgrades[level.toNumber()]) {
    return replicationUpgrades[level.toNumber()];
  }

  const upgrade: Upgrade = {
    id: `${Currency.CREDIT}-${BonusType.R_MULT}-${level}`,
    bonus: {
      type: BonusType.R_MULT,
      amount: new Decimal(0.1)
    },
    cost: {
      currency: Currency.CREDIT,
      amount: new Decimal(10).pow(level)
    },
    level,
    name: `Enhancement Unit #${level}`,
    repeating: true,
    generator: replication,
    requirements: [],
  };

  if (level.gt(1)) {
    upgrade.requirements.push(replication(level.sub(1)));
  }

  replicationUpgrades[level.toNumber()] = upgrade;

  return upgrade;
}

export function cap(level: Decimal): Upgrade {
  if (capUpgrades[level.toNumber()]) {
    return capUpgrades[level.toNumber()];
  }

  const upgrade: Upgrade = {
    id: `${Currency.CREDIT}-${BonusType.M_MULT}-${level}`,
    bonus: {
      type: BonusType.M_MULT,
      amount: new Decimal(1)
    },
    cost: {
      currency: Currency.CREDIT,
      amount: new Decimal(10).pow(level)
    },
    level,
    name: `Containment Cell #${level}`,
    repeating: true,
    generator: cap,
    requirements: [],
  };

  if (level.gt(1)) {
    upgrade.requirements.push(cap(level.sub(1)));
  }

  capUpgrades[level.toNumber()] = upgrade;

  return upgrade;
}


export function capGrowth(level: Decimal): Upgrade {
  if (capGrowthUpgrades[level.toNumber()]) {
    return capGrowthUpgrades[level.toNumber()];
  }

  const upgrade: Upgrade = {
    id: `${Currency.CREDIT}-${BonusType.T}-${level}`,
    bonus: {
      type: BonusType.T,
      amount: new Decimal(1)
    },
    cost: {
      currency: Currency.CREDIT,
      amount: new Decimal(10).pow(level)
    },
    level,
    name: `Advanced Storage Agent #${level}`,
    repeating: true,
    generator: capGrowth,
    requirements: [],
  };

  if (level.gt(1)) {
    upgrade.requirements.push(capGrowth(level.sub(1)));
  }

  capGrowthUpgrades[level.toNumber()] = upgrade;

  return upgrade;
}
