import { Component, Input, OnInit } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { clamp } from '@transistors-inc/utils';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent {
  @Input() value = 50;
  @Input() min = 0;
  @Input() max = 100;
  @Input() low?: number;
  @Input() high?: number;
  @Input() optimum?: number;

  private isActive = false;
  private offsetWidth = 0;

  removeMouseMove = () => { };
  removeMouseUp = () => { };

  constructor(private event: EventManager) { }

  handleMouseDown(ev: MouseEvent) {
    this.isActive = true;
    this.offsetWidth = (ev.currentTarget as HTMLElement).offsetWidth;
    this.removeMouseMove = this.event.addGlobalEventListener('document', 'mousemove', this.handleMouseMove.bind(this)) as () => {};
    this.removeMouseUp = this.event.addGlobalEventListener('document', 'mouseup', this.handleMouseUp.bind(this)) as () => {};
  }
  handleMouseUp(ev: MouseEvent) {
    this.isActive = false;
    this.removeMouseMove();
    this.removeMouseUp();
  }
  handleMouseMove(ev: MouseEvent) {
    if (this.isActive) {
      this.value = clamp(100 * ev.offsetX / this.offsetWidth, 0, 100);
    }
  }
}
