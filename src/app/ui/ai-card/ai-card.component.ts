import { AiModifierNames, AiModifierTypeRarities, AiTierNames } from 'src/app/data/ai.data';

import { Component, Input, OnInit } from '@angular/core';
import { Ai } from '@transistors-inc/core/ai';
import { AiModifier, AiModifierRarity, AiModifierType, AiTier } from '@transistors-inc/interfaces';
import { assertNever } from '@transistors-inc/utils';

@Component({
  selector: 'app-ai',
  templateUrl: './ai-card.component.html',
  styleUrls: ['./ai-card.component.scss']
})
export class AiCardComponent implements OnInit {
  @Input() ai!: Ai;
  @Input() generating = false;

  tierName = AiTierNames;
  constructor() { }

  ngOnInit(): void {
  }

  get rarityClass(): string {
    switch (this.ai.tier) {
      case AiTier.NUM_TIERS:
      case AiTier.UNDERGRAD: return 'undergrad';
      case AiTier.GRAD: return 'grad';
      case AiTier.PROFESSOR: return 'professor';
      case AiTier.NOBEL: return 'nobel';
      case AiTier.BREAKTHROUGH: return 'breakthrough';
      default:
        return assertNever(this.ai.tier);
    }
  }

  modName(bonus: AiModifierType): string {
    return AiModifierNames[bonus];
  }

  lineClass(line: AiModifier): string {
    if (AiModifierTypeRarities[AiModifierRarity.SPECIAL].includes(line.type)) {
      return 'special';
    }

    return 'normal';
  }
}
