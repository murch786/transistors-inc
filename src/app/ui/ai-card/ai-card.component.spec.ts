import Decimal from 'break_infinity.js';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Ai } from '@transistors-inc/core/ai';

import { AiCardComponent } from './ai-card.component';

describe('AiCardComponent', () => {
  let component: AiCardComponent;
  let fixture: ComponentFixture<AiCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AiCardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiCardComponent);
    component = fixture.componentInstance;
    component.ai = new Ai(new Decimal(0), new Decimal(0), 0, []);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
