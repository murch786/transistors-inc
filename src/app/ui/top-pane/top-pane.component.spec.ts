import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitsPipe } from '../units.pipe';
import { TopPaneComponent } from './top-pane.component';

describe('TopPaneComponent', () => {
  let component: TopPaneComponent;
  let fixture: ComponentFixture<TopPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TopPaneComponent, UnitsPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render transistors in the data pane', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('#data-pane').textContent).toContain('transistors');
  });
});
