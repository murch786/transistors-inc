import { filter, map } from 'rxjs/operators';
import { BonusType } from 'src/app/interfaces/upgrade.interface';

import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { AiService } from '@transistors-inc/core/ai.service';
import { BonusTickService } from '@transistors-inc/core/bonus-tick.service';
import { CurrencyService } from '@transistors-inc/core/currency.service';
import { KeybindService } from '@transistors-inc/core/keybind.service';
import { LogService } from '@transistors-inc/core/log.service';
import { LoopService } from '@transistors-inc/core/loop.service';
import {
  MS_PER_SEC,
  TransistorsService,
  TransistorState
} from '@transistors-inc/core/transistors.service';
import { UpgradesService } from '@transistors-inc/core/upgrades.service';
import { Currency as C } from '@transistors-inc/interfaces';
import { ProgressBarType } from '@transistors-inc/ui/progress/progress.component';
import { Decimal } from '@vendor';

const DEC_10 = new Decimal(10);

@Component({
  selector: 'app-top-pane',
  templateUrl: './top-pane.component.html',
  styleUrls: ['./top-pane.component.scss']
})
export class TopPaneComponent {
  MS_PER_SEC = MS_PER_SEC;
  TransistorState = TransistorState;
  linkIcon = faExternalLinkAlt;
  roundTimerBar = ProgressBarType.ROUND_TIMER;
  Decimal = Decimal;
  ACC_TRANSISTOR = C.ACC_TRANSISTOR;
  TRANSISTOR = C.TRANSISTOR;
  COMPUTE = C.COMPUTE;
  CREDIT = C.CREDIT;
  BONUS_TICK = C.BONUS_TICK;
  REPLICATION = BonusType.R_MULT;
  CAP = BonusType.M_MULT;
  CAP_GROWTH = BonusType.T;

  // tslint:disable-next-line: max-line-length
  get durationString() { return `${this.transistors.roundDuration?.div(MS_PER_SEC)}/${this.transistors.roundLength.div(MS_PER_SEC)} seconds`; }

  get one() { return new Decimal(1); }
  get logTen(): Decimal {
    return Decimal.floor(this.currency[C.ACC_TRANSISTOR].log10());
  }

  getSellTier(n: number): Decimal {
    return this.currency[C.ACC_TRANSISTOR].sub(DEC_10.pow(Decimal.floor(this.currency[C.ACC_TRANSISTOR].log10()).sub(n)));
  }
  get allTransistors() { return Decimal.floor(this.currency[C.ACC_TRANSISTOR]); }


  sellOne(): void { this.transistors.sell(new Decimal(1)); }
  sell(n: Decimal): void { this.transistors.sell(n); }
  sellAll(): void { this.transistors.sell(this.allTransistors); }

  creditAmount(n: Decimal = this.currency[this.ACC_TRANSISTOR]) {
    return this.transistors.baseCredits(n);
  }

  constructor(
    public loop: LoopService,
    public transistors: TransistorsService,
    public currency: CurrencyService,
    public ai: AiService,
    public upgrades: UpgradesService,
    public bonusTick: BonusTickService,
  ) { }
}
