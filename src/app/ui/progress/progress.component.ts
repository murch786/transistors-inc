import { Component, Input, OnInit } from '@angular/core';
import { clamp } from '@transistors-inc/utils';

export const enum ProgressBarType {
  ROUND_TIMER = 'round-timer'
}

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent implements OnInit {
  @Input() type = ProgressBarType.ROUND_TIMER;
  @Input() value = 0;
  @Input() min = 0;
  @Input() max = 0;
  @Input() low?: number;
  @Input() high?: number;
  @Input() optimum?: number;
  constructor() { }

  ngOnInit(): void {
  }

}
