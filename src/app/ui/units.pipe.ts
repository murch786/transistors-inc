import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { OptionsService } from '@transistors-inc/core/options.service';
import { Decimal } from '@vendor';

@Pipe({
  name: 'units'
})
export class UnitsPipe implements PipeTransform {
  private decimal: DecimalPipe = new DecimalPipe('en_us');
  constructor(private options: OptionsService) { }

  transform(value: Decimal | number, format: string): string | null {
    if (!this.options.format) { return value.toString(); }
    if (typeof value === 'number') {
      return this.decimal.transform(value, format);
    }

    return this.decimal.transform(value.toNumber(), format);
  }

}
