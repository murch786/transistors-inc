import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AiCardComponent } from './ai-card/ai-card.component';
import { DiagnosticsComponent } from './diagnostics/diagnostics.component';
import { LogComponent } from './log/log.component';
import { NavComponent } from './nav/nav.component';
import { ProgressComponent } from './progress/progress.component';
import { SliderComponent } from './slider/slider.component';
import { TopPaneComponent } from './top-pane/top-pane.component';
import { UnitsPipe } from './units.pipe';

@NgModule({
  declarations: [
    AiCardComponent,
    DiagnosticsComponent,
    LogComponent,
    NavComponent,
    ProgressComponent,
    SliderComponent,
    TopPaneComponent,
    UnitsPipe,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    AiCardComponent,
    DiagnosticsComponent,
    LogComponent,
    NavComponent,
    ProgressComponent,
    SliderComponent,
    TopPaneComponent,
    UnitsPipe,
  ]
})
export class UiModule { }
