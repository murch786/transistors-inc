import { Component, OnInit } from '@angular/core';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { LogService } from '@transistors-inc/core/log.service';
import { OptionsService } from '@transistors-inc/core/options.service';
import { LogItem, LogLevel } from '@transistors-inc/interfaces';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {
  errorIcon = faExclamationTriangle;
  errorLevel = LogLevel.ERROR;
  constructor(public log: LogService, public options: OptionsService) { }

  ngOnInit(): void {
  }

  item(i: number, item: LogItem): string { return item.id; }

}
