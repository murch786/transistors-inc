import { Component, Inject, OnInit } from '@angular/core';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { ENV, Environment } from '@transistors-inc/environment';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  linkIcon = faExternalLinkAlt;
  constructor(@Inject(ENV) public env: Environment) { }
}
