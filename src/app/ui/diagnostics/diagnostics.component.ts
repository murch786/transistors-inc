import Decimal from 'break_infinity.js';

import { Component } from '@angular/core';
import { AiService } from '@transistors-inc/core/ai.service';
import { CurrencyService } from '@transistors-inc/core/currency.service';
import { RandomService } from '@transistors-inc/core/random.service';
import { MS_PER_SEC, TransistorsService } from '@transistors-inc/core/transistors.service';
import { Currency as C } from '@transistors-inc/interfaces';

@Component({
  selector: 'app-diagnostics',
  templateUrl: './diagnostics.component.html',
  styleUrls: ['./diagnostics.component.scss']
})
export class DiagnosticsComponent {
  MS_PER_SEC = MS_PER_SEC;
  TRANSISTOR = C.TRANSISTOR;
  ACC_TRANSISTOR = C.ACC_TRANSISTOR;
  timeLeft = this.transistors.roundLength.sub(this.transistors.roundDuration ?? 0);
  randomNumber = 0;

  randomRangeResults: number[] = [];
  resultsAverage = 0;

  constructor(
    public transistors: TransistorsService,
    public currency: CurrencyService,
    private random: RandomService,
    private ai: AiService,
  ) { }

  gimmeCash() {
    this.currency.add(C.ACC_TRANSISTOR, new Decimal(10000));
  }

  gimmeBonusTicks() {
    this.currency.add(C.BONUS_TICK, new Decimal(600_000));
  }

  aiNum(): number {
    return this.ai.active.size ?? 0;
  }

  generateNumber() {
    this.randomNumber = this.random.next();
  }

  generateRange() {
    this.randomRangeResults = new Array();
    for (let i = 0; i < 1000; ++i) {
      const n = this.random.range(7);
      this.randomRangeResults[n] = (this.randomRangeResults[n] ?? 0) + 1;
    }

    this.resultsAverage = this.randomRangeResults.reduce((a, b, i) => a + b * (i + 1), 0) / (1000);
  }
}
