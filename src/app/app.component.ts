import { filter } from 'rxjs/operators';

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { KeybindService } from '@transistors-inc/core/keybind.service';
import { LogService } from '@transistors-inc/core/log.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private log: LogService,
    private keybind: KeybindService,
    private router: Router,
  ) {
    log.add('System ready');

    const keys = ['u', 'i', 'o', 'p'];

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => ev.type === 'keyup' && keys.includes(ev.key))
    ).subscribe(ev => this.keyup(ev));
  }

  keyup(ev: KeyboardEvent): void {
    switch (ev.key) {
      case 'u': this.router.navigateByUrl('upgrades'); break;
      case 'i': this.router.navigateByUrl('ais'); break;
      case 'o': this.router.navigateByUrl('options'); break;
      case 'p': this.router.navigateByUrl(''); break;
    }
  }
}
