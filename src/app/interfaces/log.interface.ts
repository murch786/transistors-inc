export const enum LogLevel {
  INFO = 'info',
  ERROR = 'error'
}

export interface LogItem {
  id: string;
  text: string;
  time: Date;
  level: LogLevel;
}
