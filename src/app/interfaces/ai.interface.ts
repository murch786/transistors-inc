import { Decimal } from '@vendor';

export const enum AiTier {
  UNDERGRAD,    // White
  GRAD,         // Green
  PROFESSOR,    // Blue
  NOBEL,        // Purple
  BREAKTHROUGH, // Orange
  NUM_TIERS
}

export const enum AiModifierRarity {
  NORMAL,
  SPECIAL
}

export const enum AiModifierType {
  // Normal modifiers
  IMPROVE_CAP,
  IMPROVE_CAP_GROWTH,
  IMPROVE_COMPUTE,
  IMPROVE_CREDIT_EXCHANGE,

  // Special modifiers
  IMPROVE_REPLICATION,
  REDUCE_MASS,
  IMPROVE_CHIP_GAIN,
  ELEVATE_COMPUTE,
  ELEVATE_CREDIT_EXCHANGE
}

export interface AiModifierData {
  min: number;
  spread: number;
}

export interface AiModifier {
  type: AiModifierType;
  amount: Decimal;
}

export interface Ai {
  id: string;
  tier: AiTier;
  lines: AiModifier[];
  baseCompute: Decimal;
  trainedCompute: Decimal;
}
