import { filter } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { AiModifierType, BonusType, Currency as C, Loop } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

import { AiService } from './ai.service';
import { BonusTickService } from './bonus-tick.service';
import { CurrencyService } from './currency.service';
import { KeybindService } from './keybind.service';
import { LogService } from './log.service';
import { LoopService } from './loop.service';
import { UpgradesService } from './upgrades.service';

export const MS_PER_SEC = new Decimal(1_000);
export enum TransistorState {
  INCREASE_SPEED = 'Run Fast',
  INCREASE_R = 'Increase R',
  INCREASE_M = 'Increase M',
}

@Injectable({
  providedIn: 'root'
})
export class TransistorsService {
  static BASE_ROUND_LENGTH = new Decimal(15);
  public seedN = new Decimal(1);
  public baseR = new Decimal(0.05);
  public startM = new Decimal(10);
  public baseM = this.startM;
  public baseT = new Decimal(1);
  public roundRunning = false;
  public roundNumber = new Decimal(0);
  public roundDuration = new Decimal(0);
  public roundLength = TransistorsService.BASE_ROUND_LENGTH.mul(MS_PER_SEC);
  public currentState = TransistorState.INCREASE_SPEED;
  public computeTotal = new Decimal(0);
  public builtAI = false;
  public useBonusTicks = false;
  public bonusTickMult = new Decimal(100);

  public lostPartialT = new Decimal(0);
  public lostAdjT = new Decimal(0);
  public gainAdjT = new Decimal(0);

  constructor(
    private loop: LoopService,
    private ai: AiService,
    private log: LogService,
    private upgrades: UpgradesService,
    private currency: CurrencyService,
    private bonusTick: BonusTickService,
    private keybind: KeybindService
  ) {
    loop.loop$.subscribe((x) => this.update(x));

    const keys = ['q', 'w', 'e', 's', 'a'];

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => ev.type === 'keyup' && keys.includes(ev.key))
    ).subscribe(ev => this.keyup(ev));
  }

  keyup(ev: KeyboardEvent): void {
    switch (ev.key) {
      case 'q': this.setState(TransistorState.INCREASE_SPEED); break;
      case 'w': this.setState(TransistorState.INCREASE_R); break;
      case 'e': this.setState(TransistorState.INCREASE_M); break;
      case 's': this.startRound(); break;
      case 'a': this.generateAI(); break;
    }
  }

  get r(): Decimal {
    return this.baseR.add(this.upgrades.getModifier(BonusType.R_ADD))
      .mul(this.ai.getModifier(AiModifierType.IMPROVE_REPLICATION))
      .mul(this.upgrades.getModifier(BonusType.R_MULT));
  }
  get m(): Decimal {
    return this.baseM.add(this.upgrades.getModifier(BonusType.M_ADD))
      .mul(this.ai.getModifier(AiModifierType.IMPROVE_CAP))
      .mul(this.upgrades.getModifier(BonusType.M_MULT));
  }
  get t(): Decimal {
    return this.baseT
      .mul(this.ai.getModifier(AiModifierType.IMPROVE_CAP_GROWTH))
      .mul(this.upgrades.getModifier(BonusType.T));
  }

  get roundUpgradeCost(): Decimal {
    return new Decimal(10).pow(this.roundLength.div(MS_PER_SEC).sub(TransistorsService.BASE_ROUND_LENGTH));
  }
  get adjR(): Decimal {
    let rate = this.r;

    if (this.currentState === TransistorState.INCREASE_R) {
      rate = rate.mul(2);
    }

    return rate;
  }
  get roundTotal(): Decimal { return Decimal.exp(this.adjR.mul(this.roundLength).div(MS_PER_SEC)).sub(this.seedN); }
  get adjTotal(): Decimal {
    if (this.roundTotal.gt(this.m)) {
      return this.m;
    } else {
      return this.roundTotal;
    }
  }

  get canStart(): boolean { return !this.roundRunning && !this.ai.running; }
  get canSell(): boolean { return this.canStart && this.roundNumber.gte(1) && this.currency[C.ACC_TRANSISTOR].gt(0); }
  get canUpgradeRound(): boolean { return this.canStart && this.currency.canAfford(C.CREDIT, this.roundUpgradeCost); }
  get canTrain(): boolean { return this.canSell && this.currency[C.ACC_TRANSISTOR].gte(1) && !this.ai.inProgress && !this.builtAI; }

  // N_t = N_(t-1) * e^((r - 1) * dT/1000)
  baseGain(interval: Decimal = MS_PER_SEC): Decimal {
    const seed = this.seedN.add(this.currency[C.TRANSISTOR]);

    return seed.mul(Decimal.exp(this.r.mul(interval).div(MS_PER_SEC))).sub(seed);
  }
  rawGain(interval: Decimal = MS_PER_SEC): Decimal {
    const seed = this.seedN.add(this.currency[C.TRANSISTOR]);

    return seed.mul(Decimal.exp(this.adjR.mul(interval).div(MS_PER_SEC))).sub(seed);
  }

  tickGain(interval: Decimal): Decimal {
    let gain = this.rawGain(interval);
    const remaining = this.m.sub(this.currency[C.TRANSISTOR]);

    if (gain.gt(remaining)) {
      gain = remaining;
    }

    return gain;
  }

  baseCompute(interval: Decimal = MS_PER_SEC, time: Decimal = this.roundDuration): Decimal {
    const r = this.r.mul(this.ai.getModifier(AiModifierType.ELEVATE_COMPUTE));
    const t = time.div(MS_PER_SEC);
    const dt = interval.div(MS_PER_SEC);
    const t0 = t.sub(dt);

    const c = this.seedN.mul(Decimal.exp(r.mul(t)).sub(r.mul(dt)).sub(Decimal.exp(r.mul(t0)))).div(r)
      .mul(this.ai.getModifier(AiModifierType.IMPROVE_COMPUTE));

    if (c.gte(this.m.mul(dt))) {
      return this.m.mul(dt);
    } else {
      return c;
    }
  }

  rawCompute(interval: Decimal = MS_PER_SEC, time: Decimal = this.roundDuration): Decimal {
    const r = this.adjR.mul(this.ai.getModifier(AiModifierType.ELEVATE_COMPUTE));
    const t = time.div(MS_PER_SEC);
    const dt = interval.div(MS_PER_SEC);
    const t0 = t.sub(dt);

    const c = this.seedN.mul(Decimal.exp(r.mul(t)).sub(r.mul(dt)).sub(Decimal.exp(r.mul(t0)))).div(r)
      .mul(this.ai.getModifier(AiModifierType.IMPROVE_COMPUTE));

    if (c.gte(this.m.mul(dt))) {
      return this.m.mul(dt);
    } else {
      return c;
    }
  }

  get roundCompute(): Decimal { return this.rawCompute(this.roundLength, this.roundLength); }

  baseCredits(n: Decimal): Decimal {
    return Decimal.floor(n)
      .mul(this.ai.getModifier(AiModifierType.IMPROVE_CREDIT_EXCHANGE))
      .pow(this.ai.getModifier(AiModifierType.ELEVATE_CREDIT_EXCHANGE));
  }

  startRound(): void {
    if (!this.canStart) { return; }
    // TODO: Add proper round interface/history
    this.roundRunning = true;
    this.roundDuration = new Decimal(0);
    this.roundNumber = this.roundNumber.add(1);
    this.upgrades.resetCompute();
    this.currency[C.COMPUTE] = new Decimal(0);
    this.computeTotal = new Decimal(0);
    this.currency[C.TRANSISTOR] = new Decimal(0);
    this.baseM = this.startM;
    this.log.add(`Round #${this.roundNumber} started`);
  }

  endRound(): void {
    this.roundRunning = false;
    this.lostPartialT = this.lostPartialT.add(this.currency[C.TRANSISTOR]).sub(Decimal.floor(this.currency[C.TRANSISTOR]));
    this.currency[C.TRANSISTOR] = Decimal.floor(this.currency[C.TRANSISTOR]);
    this.currency.add(C.ACC_TRANSISTOR, this.currency[C.TRANSISTOR]);
    this.roundDuration = this.roundLength;
    this.builtAI = false;
    // tslint:disable-next-line: max-line-length
    this.log.add(`Round #${this.roundNumber} ended, accumulating ${this.currency[C.TRANSISTOR]} transistor${this.currency[C.TRANSISTOR].gt(1) ? 's' : ''}`);
  }

  setState(state: TransistorState): void {
    this.currentState = state;
  }

  generateAI(): void {
    if (!this.canTrain) { return; }

    this.ai.build(this.currency[C.ACC_TRANSISTOR]);
    this.builtAI = true;
  }

  update(loop: Loop): void {

    let dT = new Decimal(loop.interval);

    if (this.useBonusTicks) {
      let bonusT = dT.mul(this.bonusTickMult);

      if (this.currency[C.BONUS_TICK].gt(bonusT)) {
        this.currency.subtract(C.BONUS_TICK, bonusT);
      } else {
        bonusT = this.currency[C.BONUS_TICK];
        this.currency.set(C.BONUS_TICK, new Decimal(0));
        this.useBonusTicks = false;
      }

      dT = dT.add(bonusT);
    }


    if (!this.roundRunning) { return; }

    if (this.currentState === TransistorState.INCREASE_SPEED) {
      dT = dT.mul(4);
    }

    if (this.roundDuration.add(dT).gt(this.roundLength)) {
      dT = this.roundLength.sub(this.roundDuration);
    }

    if (this.currentState === TransistorState.INCREASE_M) {
      this.baseM = this.baseM.add(this.t.mul(dT).div(MS_PER_SEC));
    }

    this.roundDuration = this.roundDuration.add(dT);

    const baseGain = this.baseGain(dT);
    const rawGain = this.rawGain(dT);
    const adjGain = this.tickGain(dT);

    if (adjGain.lt(rawGain)) {
      this.lostAdjT = this.lostAdjT.add(rawGain).sub(adjGain);
    }
    if (adjGain.gt(baseGain)) {
      this.gainAdjT = this.gainAdjT.add(adjGain).sub(baseGain);
    }

    this.currency.add(C.TRANSISTOR, adjGain, this.m);

    const computeGain = this.rawCompute(dT);
    this.computeTotal = this.computeTotal.add(computeGain);
    this.currency.add(C.COMPUTE, computeGain);

    if (this.roundDuration.gte(this.roundLength)) {
      this.endRound();
    }
  }

  sell(n: Decimal): void {
    if (this.roundNumber.lt(1)) { return; }

    this.log.add(`Sold transistors for ${n} credits`);
    this.currency.add(C.CREDIT, this.baseCredits(n));
    this.currency.subtract(C.ACC_TRANSISTOR, n);
  }

  upgradeLength(): void {
    if (this.currency.canAfford(C.CREDIT, this.roundUpgradeCost)) {
      this.currency.subtract(C.CREDIT, this.roundUpgradeCost);
      this.roundLength = this.roundLength.add(MS_PER_SEC);
      this.log.add(`Upgraded round length to ${this.roundLength.div(MS_PER_SEC)} seconds`);
    }
  }
}
