import { filter } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import * as computeUpgrades from '@transistors-inc/data/compute-upgrade.data';
import * as creditUpgrades from '@transistors-inc/data/credit-upgrade.data';
import { BonusType, Currency as C, LogLevel, Upgrade } from '@transistors-inc/interfaces';
import { assertNever } from '@transistors-inc/utils';
import { Decimal } from '@vendor';

import { CurrencyService } from './currency.service';
import { KeybindService } from './keybind.service';
import { LogService } from './log.service';
import { SerializationService } from './serialization.service';

@Injectable({
  providedIn: 'root'
})
export class UpgradesService {
  public upgrades: Set<Upgrade> = new Set();
  public available: Set<Upgrade> = new Set();
  public purchased: Set<Upgrade> = new Set();
  public totalModifiers: Map<BonusType, Decimal> = new Map();
  public upgradesBought: Record<C, number> = {
    [C.ACC_TRANSISTOR]: 0,
    [C.TRANSISTOR]: 0,
    [C.COMPUTE]: 0,
    [C.CREDIT]: 0,
    [C.BONUS_TICK]: 0
  };

  public computeUpgrades: Partial<Record<BonusType, Upgrade>> = {
    [BonusType.M_MULT]: computeUpgrades.cap(new Decimal(1)),
    [BonusType.R_MULT]: computeUpgrades.replication(new Decimal(1)),
    [BonusType.T]: computeUpgrades.capGrowth(new Decimal(1)),
  };

  constructor(
    private currency: CurrencyService,
    private serialization: SerializationService,
    private log: LogService,
    private keybind: KeybindService
  ) {
    const keys = ['1', '2', '3'];

    keybind.key$.pipe(
      filter((ev: KeyboardEvent) => ev.type === 'keydown' && keys.includes(ev.key))
    ).subscribe(ev => this.keyup(ev));

    this.upgrades.add(creditUpgrades.replication(new Decimal(1)));
    this.upgrades.add(creditUpgrades.cap(new Decimal(1)));
    this.upgrades.add(creditUpgrades.capGrowth(new Decimal(1)));

    this.initializeSavedState();
    this.resetCompute();
    this.updateAvailable();
    this.recalculate();
  }

  keyup(ev: KeyboardEvent): void {
    switch (ev.key) {
      // tslint:disable: no-non-null-assertion
      case '1': this.purchase(this.computeUpgrades[BonusType.R_MULT]!); break;
      case '2': this.purchase(this.computeUpgrades[BonusType.M_MULT]!); break;
      case '3': this.purchase(this.computeUpgrades[BonusType.T]!); break;
      // tslint:enable: no-non-null-assertion
    }
  }

  getModifier(type: BonusType): Decimal {
    if (this.totalModifiers.has(type)) {
      return this.totalModifiers.get(type) as Decimal;
    }

    let newMod: Decimal;

    switch (type) {
      case BonusType.M_ADD:
      case BonusType.R_ADD:
        newMod = new Decimal(0);
        break;
      case BonusType.M_MULT:
      case BonusType.R_MULT:
      case BonusType.T:
        newMod = new Decimal(1);
        break;
      default:
        assertNever(type);
    }

    this.totalModifiers.set(type, newMod);
    return newMod;
  }

  updateAvailable(): void {
    for (const upgrade of this.upgrades) {
      if (this.purchased.has(upgrade) || this.available.has(upgrade)) {
        this.upgrades.delete(upgrade);
        continue;
      }

      if (upgrade.requirements.every(r => this.purchased.has(r))) {
        this.available.add(upgrade);

        if (upgrade.repeating && upgrade.generator !== undefined) {
          const next = upgrade.generator(upgrade.level.add(1));
          if (!this.upgrades.has(next) && !this.available.has(next) && !this.purchased.has(next)) {
            this.upgrades.add(next);
          }
        }
      }
    }
  }

  recalculate(): void {
    this.totalModifiers.clear();

    for (const upgrade of this.purchased) {
      const mod = this.getModifier(upgrade.bonus.type);

      switch (upgrade.bonus.type) {
        case BonusType.M_ADD:
        case BonusType.R_ADD:
        case BonusType.M_MULT:
        case BonusType.R_MULT:
        case BonusType.T:
          this.totalModifiers.set(upgrade.bonus.type, mod.add(upgrade.bonus.amount));
          break;
        default:
          assertNever(upgrade.bonus.type);
      }
    }
  }

  purchase(u: Upgrade): void {
    if (!this.available.has(u)) { return; }
    if (!this.currency.canAfford(u.cost.currency, u.cost.amount)) { return; }
    if (!u.requirements.every(r => this.purchased.has(r))) {
      const missingReqs = u.requirements.filter(r => this.purchased.has(r)).map(p => p.id);
      const msg = `Attempted to purchase upgrade ${u.id} without all prereqs! ... ${missingReqs}`;
      this.log.error(msg);
      console.error(msg);
      return;
    }

    this.upgradesBought[u.cost.currency]++;
    this.currency.subtract(u.cost.currency, u.cost.amount);
    this.purchased.add(u);
    this.available.delete(u);
    this.updateAvailable();
    this.recalculate();

    if (u.repeating && u.generator !== undefined) {
      const next = u.generator(u.level.add(1));
      if (!this.upgrades.has(next) && !this.available.has(next) && !this.purchased.has(next)) {
        this.upgrades.add(next);
      }

      if (u.cost.currency === C.COMPUTE) {
        this.computeUpgrades[u.bonus.type] = u.generator(u.level.add(1));
      }
    }

    if (u.cost.currency !== C.COMPUTE) {
      this.serialization.updateState((currState) => ({
        ...currState,
        upgradeIds: [...currState.upgradeIds, u.id],
      }));
    }
  }

  resetCompute(): void {
    this.upgradesBought[C.COMPUTE] = 0;
    this.computeUpgrades = {
      [BonusType.M_MULT]: computeUpgrades.cap(new Decimal(1)),
      [BonusType.R_MULT]: computeUpgrades.replication(new Decimal(1)),
      [BonusType.T]: computeUpgrades.capGrowth(new Decimal(1)),
    };

    this.upgrades.add(computeUpgrades.replication(new Decimal(1)));
    this.upgrades.add(computeUpgrades.cap(new Decimal(1)));
    this.upgrades.add(computeUpgrades.capGrowth(new Decimal(1)));

    for (const upgrade of this.available) {
      if (upgrade.cost.currency === C.COMPUTE) {
        this.upgrades.add(upgrade);
        this.available.delete(upgrade);
      }
    }

    for (const purchase of this.purchased) {
      if (purchase.cost.currency === C.COMPUTE) {
        this.upgrades.add(purchase);
        this.purchased.delete(purchase);
      }
    }

    this.updateAvailable();
    this.recalculate();
  }

  private initializeSavedState() {
    for (const upgrade of this.available) { this.upgrades.add(upgrade); }
    for (const upgrade of this.purchased) { this.upgrades.add(upgrade); }
    this.available.clear();
    this.purchased.clear();

    this.upgradesBought = {
      [C.ACC_TRANSISTOR]: 0,
      [C.TRANSISTOR]: 0,
      [C.COMPUTE]: 0,
      [C.CREDIT]: 0,
      [C.BONUS_TICK]: 0
    };

    this.upgrades.add(creditUpgrades.replication(new Decimal(1)));
    this.upgrades.add(creditUpgrades.cap(new Decimal(1)));
    this.upgrades.add(creditUpgrades.capGrowth(new Decimal(1)));

    const state = this.serialization.loadState();

    if (!state) {
      this.updateAvailable();
      this.recalculate();
      return;
    }

    for (const upgradeId of state.upgradeIds) {
      let purchasedUpgrade: Upgrade | undefined;

      const [currency, type, level] = upgradeId.split('-');

      if (currency === C.CREDIT) {
        switch (type as BonusType) {
          case BonusType.R_MULT: purchasedUpgrade = creditUpgrades.replication(new Decimal(level)); break;
          case BonusType.M_MULT: purchasedUpgrade = creditUpgrades.cap(new Decimal(level)); break;
          case BonusType.T: purchasedUpgrade = creditUpgrades.capGrowth(new Decimal(level)); break;
        }
      }

      if (purchasedUpgrade !== undefined) {
        if (this.available.has(purchasedUpgrade)) {
          this.available.delete(purchasedUpgrade);
        }

        if (this.upgrades.has(purchasedUpgrade)) {
          this.upgrades.delete(purchasedUpgrade);
        }

        this.purchased.add(purchasedUpgrade);

        if (purchasedUpgrade.repeating && purchasedUpgrade.generator !== undefined) {
          this.upgrades.add(purchasedUpgrade.generator(purchasedUpgrade.level.add(1)));
        }
      } else {
        this.log.error(`${upgradeId} not found!`);
        console.warn(`${upgradeId} not found!`);
      }
    }

    this.updateAvailable();
    this.recalculate();
  }
}
