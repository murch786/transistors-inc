import { Injectable } from '@angular/core';
import { Currency } from '@transistors-inc/interfaces';

import { Loop } from '../interfaces/loop.interface';
import { CurrencyService } from './currency.service';
import { LoopService } from './loop.service';

@Injectable({
  providedIn: 'root'
})
export class BonusTickService {

  constructor(private currency: CurrencyService, private loop: LoopService) {
    loop.loop$.subscribe(l => this.update(l));
  }

  update({ offset }: Loop): void {
    this.currency.add(Currency.BONUS_TICK, offset);
  }

  available(): boolean {
    return this.currency[Currency.BONUS_TICK].gt(0);
  }
}
