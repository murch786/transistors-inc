import {
  animationFrameScheduler,
  ConnectableObservable,
  EMPTY,
  iif,
  interval as intervalSource,
  Observable,
  of,
  scheduled,
  Subject
} from 'rxjs';
import { map, publish, repeat, switchMap, takeWhile, tap, timeInterval } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Loop } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

import { LogService } from './log.service';

// tslint:disable: no-console
@Injectable({
  providedIn: 'root'
})
export class LoopService {
  private readonly running$: Subject<boolean> = new Subject();
  private loopSource$: Observable<Loop>;
  // tslint:disable: variable-name
  private _tick = new Decimal(0);
  private _time = new Decimal(performance.now());
  private _realTime = new Decimal(performance.now());
  private _start = new Decimal(performance.now());
  private _elapsedTime = new Decimal(0);
  // tslint:enable: variable-name
  private debug = true;
  private stepOnce = false;
  public loop$: ConnectableObservable<Loop>;

  constructor(log: LogService) {
    this.loopSource$ = scheduled([performance.now()], animationFrameScheduler).pipe(
      map(() => performance.now()),
      repeat(),
      timeInterval(animationFrameScheduler),
      map(({ value, interval }) => ({ value: new Decimal(value), interval: new Decimal(interval) })),
      map(({ value, interval }) => {
        if (interval.gt(1000)) {
          return {
            interval: new Decimal(1000),
            time: this._time,
            elapsedTime: this._elapsedTime,
            realTime: value,
            tick: this._tick,
            debug: this.debug,
            offset: interval.sub(1000)
          };
        } else {
          return {
            interval,
            time: this._time,
            elapsedTime: this._elapsedTime,
            realTime: value,
            tick: this._tick,
            debug: this.debug,
            offset: new Decimal(0)
          };
        }
      })
    );

    this.loop$ = this.running$.pipe(
      tap(x => this.debug && console.debug('running$', x)),
      tap(x => log.add(x ? 'System running' : 'System paused')),
      switchMap(active => iif(() => active, this.loopSource$, EMPTY)),
      publish<Loop>()
    ) as ConnectableObservable<Loop>;

    this.loop$.subscribe({
      next: ({ realTime, time, interval }) => {
        if (this.debug) {
          if (this._tick.gt(0) && this._tick.toNumber() % 1000 === 0) {
            console.info(`game/loop tick! (${interval}ms) ${this._tick} : ${time}`);
          }
        }

        // Should the game speed up invisibly or visibly? 🤔
        this._elapsedTime = this._elapsedTime.add(interval);
        this._time = this._time.add(interval);
        this._realTime = new Decimal(realTime);
        this._tick = this._tick.add(1);

        if (this.stepOnce) {
          this.stepOnce = false;
          this.pause();
        }
      },
      error: err => console.error('game/loop error!', err),
      complete: () => console.info('game/loop complete!')
    });

    this.loop$.connect();
    this.running$.next(true);
  }

  get tick() {
    return this._tick;
  }
  get time() {
    return this._time;
  }
  get projectedTime() {
    return this._elapsedTime;
  }
  pause() {
    this.running$.next(false);
  }
  play() {
    this.running$.next(true);
  }
  setDebug(debug: boolean) {
    this.debug = debug;
  }
  step() {
    this.stepOnce = true;
    this.play();
  }
}
