import { AiModifierNames, AiTierNames } from '@transistors-inc/data/ai.data';
import { Ai as AiInterface, AiModifier, AiTier } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

export class Ai implements AiInterface {
  id: string;
  baseCompute: Decimal;
  trainedCompute = new Decimal(0);

  readyTime: Decimal;
  complete = false;

  constructor(
    public startTime: Decimal,
    public transistors: Decimal,
    public tier: AiTier,
    public lines: AiModifier[],
  ) {
    // tslint:disable-next-line: no-bitwise
    this.id = `ai-${this.tier}-${(Math.random() * 0xFFFFFFFF | 0).toString(16).padStart(4, '0')}`;
    this.readyTime = this.startTime.add(10_000);
    this.baseCompute = transistors.mul(10_000);
  }

  train(flops: Decimal): void {
  }

  toJSON() {
    return {
      id: this.id,
      readyTime: this.readyTime.toString(),
      complete: this.complete,
      startTime: this.startTime.toString(),
      tier: AiTierNames[this.tier],
      lines: this.lines.map(line => `${AiModifierNames[line.type]}: ${line.amount.mul(100)}%`)
    };
  }
}
