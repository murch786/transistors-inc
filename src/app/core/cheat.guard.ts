import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';

import { OptionsService } from './options.service';

@Injectable({
  providedIn: 'root'
})
export class CheatGuard implements CanActivate {
  constructor(private options: OptionsService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (next.queryParamMap.has('cheat') || next.queryParamMap.has('cheats') || next.queryParamMap.has('admin')) {
      this.options.toggleCheatMode();
    }

    return true;
  }

}
