import { Injectable } from '@angular/core';
import { Currency } from '@transistors-inc/interfaces';
import { Decimal } from '@vendor';

const CREDIT: unique symbol = Symbol.for(Currency.CREDIT);
const COMPUTE: unique symbol = Symbol.for(Currency.COMPUTE);
const TRANSISTOR: unique symbol = Symbol.for(Currency.TRANSISTOR);
const ACC_TRANSISTOR: unique symbol = Symbol.for(Currency.ACC_TRANSISTOR);
const BONUS_TICK: unique symbol = Symbol.for(Currency.BONUS_TICK);

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private [CREDIT] = new Decimal(0);
  private [COMPUTE] = new Decimal(0);
  private [TRANSISTOR] = new Decimal(0);
  private [ACC_TRANSISTOR] = new Decimal(0);
  private [BONUS_TICK] = new Decimal(0);

  private get [Currency.TRANSISTOR](): Decimal { return this[TRANSISTOR]; }
  private set [Currency.TRANSISTOR](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Transistor negative! current: ${this[TRANSISTOR]}, v: ${v}`); }
    this[TRANSISTOR] = v;
  }

  private get [Currency.ACC_TRANSISTOR](): Decimal { return this[ACC_TRANSISTOR]; }
  private set [Currency.ACC_TRANSISTOR](v: Decimal) {
    // tslint:disable-next-line: max-line-length
    if (v.lt(0)) { throw new RangeError(`Trying to set Acc. Transistor negative! current: ${this[ACC_TRANSISTOR]}, v: ${v}`); }
    this[ACC_TRANSISTOR] = v;
  }

  private get [Currency.COMPUTE](): Decimal { return this[COMPUTE]; }
  private set [Currency.COMPUTE](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Compute negative! current: ${this[COMPUTE]}, v: ${v}`); }
    this[COMPUTE] = v;
  }

  private get [Currency.CREDIT](): Decimal { return this[CREDIT]; }
  private set [Currency.CREDIT](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Credit negative! current: ${this[CREDIT]}, v: ${v}`); }
    this[CREDIT] = v;
  }

  private get [Currency.BONUS_TICK](): Decimal { return this[BONUS_TICK]; }
  private set [Currency.BONUS_TICK](v: Decimal) {
    if (v.lt(0)) { throw new RangeError(`Trying to set Bonus Tick negative! current: ${this[BONUS_TICK]}, v: ${v}`); }
    this[BONUS_TICK] = v;
  }

  canAfford(currency: Currency, amount: Decimal): boolean {
    return this[currency].gte(amount);
  }

  add(currency: Currency, amount: Decimal, max?: Decimal): void {
    if (amount.lt(0)) { throw new RangeError(`Cannot add ${amount} currency to ${currency}`); }
    if (max !== undefined && this[currency].add(amount).gt(max)) {
      this[currency] = new Decimal(max);
    } else {
      this[currency] = this[currency].add(amount);
    }
  }

  subtract(currency: Currency, amount: Decimal): void {
    if (amount.lte(0)) { throw new RangeError(`Cannot subtract ${amount} currency from ${currency}`); }
    if (amount.gt(this[currency])) {
      // tslint:disable-next-line: max-line-length
      throw new Error(`Cannot afford to subtract ${amount} from ${currency}, only ${this[currency]} available`);
    }

    this[currency] = this[currency].sub(amount);
  }

  set(currency: Currency, amount: Decimal): void {
    if (amount.lt(0)) { throw new RangeError(`Cannot set ${currency} to negative amount ${amount}`); }

    this[currency] = new Decimal(amount);
  }
}
