import { TestBed } from '@angular/core/testing';

import { BonusTickService } from './bonus-tick.service';

describe('BonusTickService', () => {
  let service: BonusTickService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BonusTickService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
