import Decimal from 'break_infinity.js';

import { TestBed } from '@angular/core/testing';
import { cap, replication } from '@transistors-inc/data/credit-upgrade.data';

import { SerializationService } from './serialization.service';
import { UpgradesService } from './upgrades.service';

describe('UpgradeService', () => {
  let serialization: SerializationService;
  let service: UpgradesService;

  beforeEach(() => {
    localStorage.clear();
    TestBed.configureTestingModule({});
    serialization = TestBed.inject(SerializationService);
  });

  it('should be created', () => {
    service = TestBed.inject(UpgradesService);
    expect(service).toBeTruthy();
    expect(service.purchased.size).toEqual(0);
  });

  describe('saved upgrades', () => {
    beforeEach(() => {
      serialization.saveState({
        upgradeIds: ['credit-r_mult-1', 'credit-m_mult-1'],
      });
    });

    it('should load the saved, purchased upgrades when constructed', () => {
      service = TestBed.inject(UpgradesService);

      expect(service.purchased.size).toEqual(2);
      expect(service.available.has(replication(new Decimal(2)))).toBeTrue();
      expect(service.available.has(cap(new Decimal(2)))).toBeTrue();
    });
  });
});
