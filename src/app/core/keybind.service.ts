import { fromEventPattern, merge, Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';

import { OptionsService } from './options.service';

export type KeyboardHandler = (ev: KeyboardEvent) => void;

@Injectable({
  providedIn: 'root'
})
export class KeybindService {
  private keyDown$: Observable<KeyboardEvent>;
  private keyUp$: Observable<KeyboardEvent>;
  public readonly key$: Observable<KeyboardEvent>;

  private removeKeydownHandler: () => void = () => { };
  private removeKeyupHandler: () => void = () => { };

  constructor(private events: EventManager, private options: OptionsService) {
    this.keyDown$ = fromEventPattern(this.addKeydownHandler.bind(this), this.removeKeydownHandler.bind(this));
    this.keyUp$ = fromEventPattern(this.addKeyupHandler.bind(this), this.removeKeyupHandler.bind(this));
    this.key$ = merge(this.keyDown$, this.keyUp$);
  }

  addKeydownHandler(handler: KeyboardHandler) {
    this.removeKeydownHandler = this.events.addGlobalEventListener('window', 'keydown', handler) as () => void;
  }

  addKeyupHandler(handler: KeyboardHandler) {
    this.removeKeyupHandler = this.events.addGlobalEventListener('window', 'keyup', handler) as () => void;
  }
}
