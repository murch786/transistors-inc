import { Injectable } from '@angular/core';
import {
  AiModifierRanges,
  AiModifierTypeRarities as ModTypeRarities
} from '@transistors-inc/data/ai.data';
import {
  AiModifier,
  AiModifierRarity as ModRarity,
  AiModifierType,
  AiTier,
  Currency,
  Loop
} from '@transistors-inc/interfaces';
import { assertNever } from '@transistors-inc/utils';
import { findTier } from '@transistors-inc/utils/ai';
import { Decimal } from '@vendor';

import { Ai } from './ai';
import { LogService } from './log.service';
import { LoopService } from './loop.service';
import { RandomService } from './random.service';
import { UpgradesService } from './upgrades.service';

@Injectable({
  providedIn: 'root'
})
export class AiService {
  private generating?: Ai;
  private training?: Ai;
  private trainingStart?: Decimal;
  private trainingStrength?: Decimal;
  public active: Set<Ai> = new Set();

  public totalModifiers: Map<AiModifierType, Decimal> = new Map();

  constructor(
    private loop: LoopService,
    private log: LogService,
    private random: RandomService,
    private upgrades: UpgradesService,
  ) {
    loop.loop$.subscribe((x) => this.update(x));
  }

  get inProgress(): boolean { return this.generating !== undefined; }
  get generatingAI(): Readonly<Ai | undefined> { return this.generating; }
  get canTrain(): boolean { return this.training === undefined; }
  get running(): boolean { return this.inProgress || !this.canTrain; }
  get f(): number { return Math.max(0.8 - 0.01 * this.upgrades.upgradesBought[Currency.CREDIT] / 10); }

  getModifier(type: AiModifierType): Decimal {
    if (this.totalModifiers.has(type)) {
      return this.totalModifiers.get(type) as Decimal;
    }

    const newMod = new Decimal(1);
    this.totalModifiers.set(type, newMod);
    return newMod;
  }

  recalculate(): void {
    this.totalModifiers.clear();

    for (const ai of this.active) {
      for (const line of ai.lines) {
        const mod = this.getModifier(line.type);
        switch (line.type) {
          // Normal modifiers
          case AiModifierType.IMPROVE_CAP:
          case AiModifierType.IMPROVE_CAP_GROWTH:
          case AiModifierType.IMPROVE_COMPUTE:
          case AiModifierType.IMPROVE_CREDIT_EXCHANGE:

          // Special modifiers
          case AiModifierType.IMPROVE_REPLICATION:
          case AiModifierType.REDUCE_MASS:
          case AiModifierType.IMPROVE_CHIP_GAIN:
          case AiModifierType.ELEVATE_COMPUTE:
          case AiModifierType.ELEVATE_CREDIT_EXCHANGE:
            this.totalModifiers.set(line.type, mod.add(line.amount));
            break;
          default:
            assertNever(line.type);
        }
      }
    }
  }

  createAI(n: Decimal = new Decimal(1)): Ai {
    const tier = findTier(this.random.next(), this.f);
    const lines: AiModifier[] = [];

    for (let line = 0; line < tier + 1; ++line) {
      let rarity = ModRarity.NORMAL;
      const rarityRoll = this.random.next();

      // Line #3 always has a 20% chance of being special.
      // Lines #4 and #5 are always special.
      if (line === 2 && rarityRoll < 0.2 || line >= 3) {
        rarity = ModRarity.SPECIAL;
      }
      const choices = ModTypeRarities[rarity].filter((modType) => !lines.map(lineMod => lineMod.type).includes(modType));
      const choice = this.random.range(choices.length);
      const type = choices[choice];
      const base = AiModifierRanges[type].min * Decimal.floor(n.log10() + 1).toNumber();
      const mod = this.random.range(AiModifierRanges[type].spread) * Decimal.floor(n.log10() + 1).toNumber();

      lines[line] = {
        type,
        amount: new Decimal((base + mod) / 100).mul(rarity === ModRarity.NORMAL ? tier + 1 : 1)
      };
    }

    return new Ai(this.loop.time, n, tier, lines);
  }

  build(n: Decimal): void {
    this.log.add(`Training new AI [f:${this.f}]`);
    this.generating = this.createAI(n);
  }

  advanceGeneration(time: Decimal): void {
    if (this.generating !== undefined) {
      this.generating.readyTime = this.generating?.readyTime.sub(time);
    }
  }

  beginTraining(n: Decimal, ai: Ai): void {
    this.training = ai;
    this.trainingStart = this.loop.time;
    this.trainingStrength = n;
  }

  endTraining(): void {
    if (this.training !== undefined && this.trainingStrength !== undefined) {
      this.training.train(this.trainingStrength.mul(this.loop.time.sub(this.trainingStrength)));
    }
  }

  update(loop: Loop): void {
    if (this.generating !== undefined && loop.time.gte(this.generating?.readyTime)) {
      this.log.add(`AI ${this.generating.id} ready`);
      this.generating.complete = true;
      this.active.add(this.generating);
      delete this.generating;
      this.recalculate();
    }
  }
}
