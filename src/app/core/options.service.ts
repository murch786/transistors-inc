import { Injectable } from '@angular/core';

import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class OptionsService {
  public format = true;
  public logLength = 15;
  private cheatState = false;

  get cheatMode() { return this.cheatState; }

  constructor(private log: LogService) { }

  toggleCheatMode(): void {
    this.cheatState = !this.cheatState;
    this.log.add(`Cheat mode ${this.cheatState ? 'enabled' : 'disabled'}`);
  }
}
