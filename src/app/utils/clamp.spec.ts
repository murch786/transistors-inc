import { clamp } from './clamp';

describe('utils/clamp', () => {
  it('returns n if between min and max', () => {
    expect(clamp(5, 0, 10)).toBe(5);
  });

  it('returns min if n less than min', () => {
    expect(clamp(0, 5, 10)).toBe(5);
  });

  it('returns max if n greater than max', () => {
    expect(clamp(10, 0, 5)).toBe(5);
  });
});
