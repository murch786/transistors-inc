import { Component, OnInit } from '@angular/core';
import { AiService } from '@transistors-inc/core/ai.service';
import { OptionsService } from '@transistors-inc/core/options.service';
import { AiModifierNames } from '@transistors-inc/data/ai.data';
import { AiModifierType, AiTier } from '@transistors-inc/interfaces';
import { assertNever } from '@transistors-inc/utils';

@Component({
  selector: 'app-ai-list',
  templateUrl: './ai-list.component.html',
  styleUrls: ['./ai-list.component.scss']
})
export class AiListComponent implements OnInit {
  public AiModifierNames = AiModifierNames;
  constructor(public ais: AiService, public options: OptionsService) { }

  ngOnInit(): void {
  }

  getModifierName(type: AiModifierType): string {
    return AiModifierNames[type];
  }

  rarityClass(tier: AiTier): string {
    switch (tier) {
      case AiTier.NUM_TIERS:
      case AiTier.UNDERGRAD: return 'undergrad';
      case AiTier.GRAD: return 'grad';
      case AiTier.PROFESSOR: return 'professor';
      case AiTier.NOBEL: return 'nobel';
      case AiTier.BREAKTHROUGH: return 'breakthrough';
      default:
        return assertNever(tier);
    }
  }

  addHundo(): void {
    for (let i = 0; i < 100; ++i) {
      this.ais.active.add(this.ais.createAI());
    }
    this.ais.recalculate();
  }

  murderUndergrads(): void {
    for (const ai of this.ais.active) {
      if (ai.tier === AiTier.UNDERGRAD) {
        this.ais.active.delete(ai);
      }
    }

    this.ais.recalculate();
  }

  murderGrads(): void {
    for (const ai of this.ais.active) {
      if (ai.tier === AiTier.GRAD) {
        this.ais.active.delete(ai);
      }
    }

    this.ais.recalculate();
  }
}
