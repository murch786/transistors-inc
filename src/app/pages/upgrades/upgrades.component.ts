import { BonusTypeNames } from 'src/app/data/upgrade.data';

import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '@transistors-inc/core/currency.service';
import { UpgradesService } from '@transistors-inc/core/upgrades.service';
import { BonusType } from '@transistors-inc/interfaces';

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.scss']
})
export class UpgradesComponent implements OnInit {

  constructor(public upgrades: UpgradesService, public currency: CurrencyService) { }

  ngOnInit(): void {
  }

  getModifierName(type: BonusType): string {
    return BonusTypeNames[type];
  }
}
