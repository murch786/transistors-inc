import { MarkdownModule } from 'ngx-markdown';

import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UiModule } from '@transistors-inc/ui/ui.module';

import { AiListComponent } from './ai-list/ai-list.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { DesignComponent } from './design/design.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OptionsComponent } from './options/options.component';
import { PagesRoutingModule } from './pages-routing.module';
import { UpgradesComponent } from './upgrades/upgrades.component';

@NgModule({
  declarations: [
    AiListComponent,
    ChangelogComponent,
    DesignComponent,
    HomeComponent,
    NotFoundComponent,
    OptionsComponent,
    UpgradesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FontAwesomeModule,
    MarkdownModule.forRoot({ loader: HttpClient }),
    UiModule
  ]
})
export class PagesModule { }
