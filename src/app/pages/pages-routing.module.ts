import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheatGuard } from '@transistors-inc/core/cheat.guard';

import { AiListComponent } from './ai-list/ai-list.component';
import { ChangelogComponent } from './changelog/changelog.component';
import { DesignComponent } from './design/design.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OptionsComponent } from './options/options.component';
import { UpgradesComponent } from './upgrades/upgrades.component';

export const routes: Routes = [
  {
    path: '', canActivate: [CheatGuard], children: [
      { path: '', component: HomeComponent },
      { path: 'upgrades', component: UpgradesComponent },
      { path: 'ais', component: AiListComponent },
      { path: 'design', component: DesignComponent },
      { path: 'changelog', component: ChangelogComponent },
      { path: 'options', component: OptionsComponent },
      { path: '**', component: NotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
