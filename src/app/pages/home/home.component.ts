import { Component } from '@angular/core';
import { OptionsService } from '@transistors-inc/core/options.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  constructor(public options: OptionsService) { }
}
