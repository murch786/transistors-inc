import { Component } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    @Component({ selector: 'app-log', template: '' })
    class LogComponentStub { }

    @Component({ selector: 'app-diagnostics', template: '' })
    class DiagnosticsComponentStub { }

    TestBed.configureTestingModule({
      declarations: [HomeComponent, LogComponentStub, DiagnosticsComponentStub]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
