import { Component, OnInit } from '@angular/core';
import { OptionsService } from '@transistors-inc/core/options.service';
import { SerializationService } from '@transistors-inc/core/serialization.service';
import { UpgradesService } from '@transistors-inc/core/upgrades.service';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

  constructor(
    public options: OptionsService,
    private serializer: SerializationService,
    private upgrades: UpgradesService,
  ) { }

  ngOnInit(): void {
  }

  reset() {
    this.serializer.resetState();
    // tslint:disable-next-line: no-string-literal
    this.upgrades['initializeSavedState']();
  }

}
